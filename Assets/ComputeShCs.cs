﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputeShCs : MonoBehaviour {
	public ComputeShader computeShader;

	public Renderer rendererA;
	public Renderer rendererB;

	public Texture2D Texture_A;
	RenderTexture renderTexture;
	RenderTexture renderTexture2;
	public UnityEngine.UI.RawImage rawImage;

	int kernelIndex_KernelFunction_A;

	struct ThreadSize
	{
		public int x;
		public int y;
		public int z;

		public ThreadSize(uint x, uint y, uint z)
		{
			this.x = (int)x;
			this.y = (int)y;
			this.z = (int)z;
		}
	}

	ThreadSize kernelThreadSize_KernelFunction_A;

	void Start()
	{
		// enableRandomWrite を有効にして、
		// 書き込み可能な状態のテクスチャを生成することが重要です。

		/*
		this.Texture_A = new Texture2D(1920 / 8, 1080 / 8);
		Color[] color = new Color[1920 / 8 * 1080 / 8];
		Array.ForEach(color, r => r = Color.Lerp(Color.white,Color.black,UnityEngine.Random.Range(0,1.0f)));
		Texture_A.SetPixels(color);
		Texture_A.Apply();
		*/

		renderTexture = new RenderTexture(Texture_A.width, Texture_A.height, 0, RenderTextureFormat.ARGB32);
		renderTexture.enableRandomWrite = true;
		renderTexture.Create();

		renderTexture2 = new RenderTexture(Texture_A.width, Texture_A.height, 0, RenderTextureFormat.ARGB32);
		renderTexture2.enableRandomWrite = true;
		renderTexture2.Create();
		// カーネルのインデックスと、スレッドのサイズを保存します。

		this.kernelIndex_KernelFunction_A
			= this.computeShader.FindKernel("KernelFunction_A");

		uint threadSizeX, threadSizeY, threadSizeZ;

		this.computeShader.GetKernelThreadGroupSizes
			(this.kernelIndex_KernelFunction_A,
			 out threadSizeX, out threadSizeY, out threadSizeZ);

		this.kernelThreadSize_KernelFunction_A
			= new ThreadSize(threadSizeX, threadSizeY, threadSizeZ);

		// ComputeShader で計算した結果を保存するためのバッファとして、ここではテクスチャを設定します。
		this.computeShader.SetTexture
			(this.kernelIndex_KernelFunction_A, "textureBufferA", this.renderTexture);
		this.computeShader.SetTexture
			(this.kernelIndex_KernelFunction_A, "InputTexture", this.Texture_A);
		this.computeShader.Dispatch(this.kernelIndex_KernelFunction_A,
									this.Texture_A.width / this.kernelThreadSize_KernelFunction_A.x,
									this.Texture_A.height / this.kernelThreadSize_KernelFunction_A.y,
									this.kernelThreadSize_KernelFunction_A.z);
		rendererA.material.mainTexture = Texture_A;
		StartCoroutine(CellUpdate());
	}
	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			//((Texture2D)((Texture)renderTexture_A)).SetPixel((int)Input.mousePosition.x * renderTexture_A.width, (int)Input.mousePosition.y * renderTexture_A.height, Color.white);
		}
	}
	IEnumerator CellUpdate()
	{
		// Dispatch メソッドで ComputeShader を実行します。
		// ここで、グループ数は、"解像度 / スレッド数" で算出します。
		// これですべてのピクセルが処理されます。
		// 
		// 例えば 512 / 8 = 64 です。各グループは 64 ピクセルずつ平行に処理します。
		// またグループスレッドは、64 / 8 = 8 ピクセルずつ並行に処理します。
		while (true)
		{
			//yield return new WaitForSeconds(1);
			this.computeShader.SetTexture
			(this.kernelIndex_KernelFunction_A, "textureBufferA", this.renderTexture2);
			this.computeShader.SetTexture
				(this.kernelIndex_KernelFunction_A, "InputTexture", this.renderTexture);
			this.computeShader.Dispatch(this.kernelIndex_KernelFunction_A,
										this.Texture_A.width / this.kernelThreadSize_KernelFunction_A.x,
										this.Texture_A.height / this.kernelThreadSize_KernelFunction_A.y,
										this.kernelThreadSize_KernelFunction_A.z);
			//rendererB.material.mainTexture = renderTexture2;
			rawImage.material.mainTexture = renderTexture2;
			//yield return new WaitForSeconds(2);
			yield return null;
			this.computeShader.SetTexture
			(this.kernelIndex_KernelFunction_A, "textureBufferA", this.renderTexture);
			this.computeShader.SetTexture
				(this.kernelIndex_KernelFunction_A, "InputTexture", this.renderTexture2);
			this.computeShader.Dispatch(this.kernelIndex_KernelFunction_A,
										this.Texture_A.width / this.kernelThreadSize_KernelFunction_A.x,
										this.Texture_A.height / this.kernelThreadSize_KernelFunction_A.y,
										this.kernelThreadSize_KernelFunction_A.z);
			//rendererB.material.mainTexture = renderTexture;
			rawImage.material.mainTexture = renderTexture;
			//yield return new WaitForSeconds(2);
			yield return null;
			//Debug.Log("Loop");
		}
	}
}
